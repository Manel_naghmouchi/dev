const express=require('express');
const app=express();
const mongoose=require('mongoose');
const bodyParser = require("body-parser");
const cors = require("cors");

//import routes
const authRoute = require('./routes/auth');




app.use('/api/user',authRoute);
app.use(cors());
app.use(express.json());



app.get("/", (req, res) => {
      res.json({ message: "Welcome To The Server." });
    });

    /* Connection à MongoDb*/

    mongoose.connect(`mongodb://localhost:27017/er`,(err)=>{
        if(err){ console.log(err); }
        else{ console.log("Mongo db connection sucess"); }
        });
        

app.listen(8000, ()=>console.log('server up and running'));