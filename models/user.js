const mongoose=require('mongoose');
const userSchema=new mongoose.Schema({

nom:{
    type:String,
    required:true,
    min:6,
    max:255
},
prenom:{
    type:String,
    required:true,
    min:6,
    max:255
},
email:{
    type:String,
    required:true,
    max:255,
    min:6
},
adresse:{
    type:String,
    required:true,
    max:255,
    min:6
},
tel:{
    type:String,
    required:true
},
motpasse:{
    type:String,
    required:true,
    max:1024,
    min:6
},
date:{
    type: Date,
    default: Date.now
}






});
module.exports=mongoose.model('user',userSchema);